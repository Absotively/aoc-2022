import { open } from 'node:fs/promises';

const file = await open('./input');
const input = await file.readFile('ascii');

class Coord {
  constructor(row, column) {
    this.row = row;
    this.column = column;
  }
  
  equals(other) {
    return this.row == other.row && this.column == other.column;
  }
}

class Grid {
  constructor(textGrid) {
    let row = 0;
    this.elevations = new Array();
    this.distancesFromStartingPoint = new Array();
    const charCodeA = 'a'.charCodeAt(0);
    for (const line of textGrid.split('\n')) {
      let rowArray = new Array();
      let column = 0;
      for (const c of line) {
        if (c == 'S') {
          this.startingPoint = new Coord(row, column);
          rowArray.push(0);
        } else if (c == 'E') {
          this.destination = new Coord(row, column);
          rowArray.push(25);
        } else {
          rowArray.push(c.charCodeAt(0) - charCodeA);
        }
        column++;
      }
      this.elevations.push(rowArray);
      row++;
    }
    this.height = this.elevations.length;
    this.width = this.elevations[0].length;
    this.distancesFromStartingPoint = new Array();
    for (let r = 0; r < this.height; r++) {
      this.distancesFromStartingPoint.push(new Array(this.width));
    }
    this.distancesFromStartingPoint[this.startingPoint.row][this.startingPoint.column] = 0;
  }
  
  elevation(coord) {
    return this.elevations[coord.row][coord.column];
  }
  
  distance(coord) {
    return this.distancesFromStartingPoint[coord.row][coord.column];
  }
}

const directions = new Map([
  ['u', {row: -1, column: 0}],
  ['l', {row: 0, column: -1}],
  ['r', {row: 0, column: 1}],
  ['d', {row: 1, column: 0}]
]);

let grid = new Grid(input);

let currentDistance = 0;
let pointsAtCurrentDistance = [grid.startingPoint];
let pointsAtNextDistance = [];

while(pointsAtCurrentDistance.length != 0) {
  for (const point of pointsAtCurrentDistance) {
    let currentElevation = grid.elevation(point);
    for (const directionOffset of directions.values()) {
      let nextPoint = new Coord(point.row + directionOffset.row, point.column + directionOffset.column);
      if (nextPoint.row >= 0 && nextPoint.row < grid.height
          && nextPoint.column >= 0 && nextPoint.column < grid.width
          && grid.distance(nextPoint) === undefined
          && grid.elevation(nextPoint) - currentElevation <= 1)
      {
        grid.distancesFromStartingPoint[nextPoint.row][nextPoint.column] = currentDistance + 1;
        pointsAtNextDistance.push(nextPoint);
      }
    }
  }
  currentDistance++;
  pointsAtCurrentDistance = pointsAtNextDistance;
  pointsAtNextDistance = [];
}

console.log(grid.distancesFromStartingPoint[grid.destination.row][grid.destination.column]);