import { open } from 'node:fs/promises';

const file = await open('./input');
const input = await file.readFile('ascii');

class Coord {
  constructor(row, column) {
    this.row = row;
    this.column = column;
  }
  
  equals(other) {
    return this.row == other.row && this.column == other.column;
  }
}

class Grid {
  constructor(textGrid) {
    let row = 0;
    this.elevations = new Array();
    const charCodeA = 'a'.charCodeAt(0);
    for (const line of textGrid.split('\n')) {
      let rowArray = new Array();
      let column = 0;
      for (const c of line) {
        if (c == 'S') {
          this.startingPoint = new Coord(row, column);
          rowArray.push(0);
        } else if (c == 'E') {
          this.destination = new Coord(row, column);
          rowArray.push(25);
        } else {
          rowArray.push(c.charCodeAt(0) - charCodeA);
        }
        column++;
      }
      this.elevations.push(rowArray);
      row++;
    }
    this.height = this.elevations.length;
    this.width = this.elevations[0].length;
    this.distancesFromDestination = new Array();
    for (let r = 0; r < this.height; r++) {
      this.distancesFromDestination.push(new Array(this.width));
    }
    this.distancesFromDestination[this.destination.row][this.destination.column] = 0;
  }
  
  elevation(coord) {
    return this.elevations[coord.row][coord.column];
  }
  
  distance(coord) {
    return this.distancesFromDestination[coord.row][coord.column];
  }
}

const directions = new Map([
  ['u', {row: -1, column: 0}],
  ['l', {row: 0, column: -1}],
  ['r', {row: 0, column: 1}],
  ['d', {row: 1, column: 0}]
]);

let grid = new Grid(input);

let currentDistance = 0;
let pointsAtCurrentDistance = [grid.destination];
let pointsAtNextDistance = [];
let shortestPathLength = undefined;

while(pointsAtCurrentDistance.length != 0 && shortestPathLength === undefined) {
  for (const point of pointsAtCurrentDistance) {
    let currentElevation = grid.elevation(point);
    for (const directionOffset of directions.values()) {
      let nextPoint = new Coord(point.row + directionOffset.row, point.column + directionOffset.column);
      if (nextPoint.row >= 0 && nextPoint.row < grid.height
          && nextPoint.column >= 0 && nextPoint.column < grid.width
          && grid.distance(nextPoint) === undefined
          && currentElevation - grid.elevation(nextPoint) <= 1)
      {
        grid.distancesFromDestination[nextPoint.row][nextPoint.column] = currentDistance + 1;
        pointsAtNextDistance.push(nextPoint);
        if (grid.elevation(nextPoint) == 0) {
          shortestPathLength = currentDistance + 1;
        }
      }
    }
  }
  currentDistance++;
  pointsAtCurrentDistance = pointsAtNextDistance;
  pointsAtNextDistance = [];
}

console.log(shortestPathLength);

// 14 is wrong