import { open } from 'node:fs/promises';

const file = await open('./input');

let locationsTailVisited = new Set();

// x,y
let knotLocations = new Array();
for (let i = 0; i < 10; i++) {
  knotLocations[i] = new Array(0,0);
} 

function move(dir) {
  switch (dir) {
  case 'U':
    knotLocations[0][1]++;
    break;
  case 'D':
    knotLocations[0][1]--;
    break;
  case 'L':
    knotLocations[0][0]--;
    break;
  case 'R':
    knotLocations[0][0]++;
    break;
  }
  
  for (let i = 1; i < 10; i++) {
    if (knotLocations[i-1][0] - knotLocations[i][0] > 1) {
      knotLocations[i][0]++;
      if (knotLocations[i-1][1] > knotLocations[i][1]) {
        knotLocations[i][1]++;
      }
      else if (knotLocations[i-1][1] < knotLocations[i][1]) {
        knotLocations[i][1]--;
      }
    }
    else if (knotLocations[i][0] - knotLocations[i-1][0] > 1) {
      knotLocations[i][0]--;
      if (knotLocations[i-1][1] > knotLocations[i][1]) {
        knotLocations[i][1]++;
      }
      else if (knotLocations[i-1][1] < knotLocations[i][1]) {
        knotLocations[i][1]--;
      }
    }
    else if (knotLocations[i-1][1] - knotLocations[i][1] > 1) {
      knotLocations[i][1]++;
      if (knotLocations[i-1][0] > knotLocations[i][0]) {
        knotLocations[i][0]++;
      }
      else if (knotLocations[i-1][0] < knotLocations[i][0]) {
        knotLocations[i][0]--;
      }
    }
    else if (knotLocations[i][1] - knotLocations[i-1][1] > 1) {
      knotLocations[i][1]--;
      if (knotLocations[i-1][0] > knotLocations[i][0]) {
        knotLocations[i][0]++;
      }
      else if (knotLocations[i-1][0] < knotLocations[i][0]) {
        knotLocations[i][0]--;
      }
    }
  }

  locationsTailVisited.add(`${knotLocations[9][0]},${knotLocations[9][1]}`);
}

const inputLineRE = /(?<dir>U|L|R|D) (?<count>\d+)/;

for await (const line of file.readLines()) {
  let match = inputLineRE.exec(line);
  for (let i = 0; i < match.groups.count; i++) {
    move(match.groups.dir);
  }
}

console.log(locationsTailVisited.size);

// 2655 is wrong