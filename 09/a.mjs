import { open } from 'node:fs/promises';

const file = await open('./input');

let locationsTailVisited = new Set();

// x,y
let headLocation = new Array(0,0);
let tailLocation = new Array(0,0);

locationsTailVisited.add('0,0');

function move(dir) {
  switch (dir) {
  case 'U':
    headLocation[1]++;
    break;
  case 'D':
    headLocation[1]--;
    break;
  case 'L':
    headLocation[0]--;
    break;
  case 'R':
    headLocation[0]++;
    break;
  }
  
  if (headLocation[0] - tailLocation[0] > 1) {
    tailLocation[0]++;
    if (headLocation[1] != tailLocation[1]) {
      tailLocation[1] = headLocation[1];
    }
  }
  else if (tailLocation[0] - headLocation[0] > 1) {
    tailLocation[0]--;
    if (headLocation[1] != tailLocation[1]) {
      tailLocation[1] = headLocation[1];
    }
  }
  else if (headLocation[1] - tailLocation[1] > 1) {
    tailLocation[1]++;
    if (headLocation[0] != tailLocation[0]) {
      tailLocation[0] = headLocation[0];
    }
  }
  else if (tailLocation[1] - headLocation[1] > 1) {
    tailLocation[1]--;
    if (headLocation[0] != tailLocation[0]) {
      tailLocation[0] = headLocation[0];
    }
  }
  
  locationsTailVisited.add(`${tailLocation[0]},${tailLocation[1]}`);
}

const inputLineRE = /(?<dir>U|L|R|D) (?<count>\d+)/;

for await (const line of file.readLines()) {
  let match = inputLineRE.exec(line);
  for (let i = 0; i < match.groups.count; i++) {
    move(match.groups.dir);
  }
}

console.log(locationsTailVisited.size);