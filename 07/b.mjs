import { open } from 'node:fs/promises';

const file = await open('./input');

const lineRE = /(\$ (?<ls>ls)|(cd (?<destDir>.+)))|(dir (?<dirListing>.*))|((?<fileSize>\d+) .*)/

// I should learn to make prototypes or whatever
function makeDirObject(parent) {
  let dirObject = new Object();
  dirObject.parent = parent;
  dirObject.children = new Map();
  dirObject.directFileSum = 0;
  return dirObject;
}

function getDirSize(dir) {
  if (dir.totalSize !== undefined) {
    return dir.totalSize;
  }
  let size = dir.directFileSum;
  for (let child of dir.children) {
    size += getDirSize(child[1]);
  }
  dir.totalSize = size;
  return size;
}

let rootDir = makeDirObject(null);
let currentDir = rootDir;

for await (const line of file.readLines()) {
  let match = lineRE.exec(line);
  if (match.groups.destDir == '/') {
    currentDir = rootDir;
  }
  else if (match.groups.destDir == '..') {
    currentDir = currentDir.parent;
  }
  else if (match.groups.destDir !== undefined) {
    currentDir = currentDir.children.get(match.groups.destDir);
  }
  else if (match.groups.fileSize !== undefined) {
    currentDir.directFileSum += parseInt(match.groups.fileSize);
  }
  else if (match.groups.dirListing !== undefined) {
    currentDir.children.set(match.groups.dirListing, makeDirObject(currentDir));
  }
  else if (match.groups.ls !== undefined) {
    // in case a dir is listed twice, though probably none are
    currentDir.directFileSum = 0;
  }
  else {
    console.log("line not understood");
  }
}

let spaceAvailable = 70000000 - getDirSize(rootDir);
let spaceNeeded = 30000000 - spaceAvailable;

let bestCandidateSize = 70000000;

function checkCandidates(dir) {
  let dirSize = getDirSize(dir);
  if (dirSize >= spaceNeeded && dirSize < bestCandidateSize) {
    bestCandidateSize = dirSize;
  }
  for (let child of dir.children) {
    checkCandidates(child[1]);
  }
}

checkCandidates(rootDir);

console.log(bestCandidateSize);

// 34257857 is wrong

