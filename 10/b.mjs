import { open } from 'node:fs/promises';

const file = await open('./input');

let currentCycle = 1;
let registerX = 1;
let screen = '';

function drawAndAdvance() {
  let currentHorizontalPosition = (currentCycle - 1) % 40;
  if (registerX == currentHorizontalPosition
      || registerX + 1 == currentHorizontalPosition
      || registerX - 1 == currentHorizontalPosition) {
    screen += '#';
  } else {
    screen += '.';
  }
  if (currentHorizontalPosition == 39) {
    screen += '\n';
  }
  currentCycle++;
}

for await (const line of file.readLines()) {
  if (line == 'noop') {
    drawAndAdvance();
  }
  else {
    drawAndAdvance();
    drawAndAdvance();
    registerX += parseInt(line.slice(5));
  }
}

console.log(screen);