import { open } from 'node:fs/promises';

const file = await open('./input');

let currentCycle = 1;
let registerX = 1;

let relevantCycles = new Set([20,60,100,140,180,220]);
let signalStrengthSum = 0;

for await (const line of file.readLines()) {
  if (relevantCycles.has(currentCycle)) {
    signalStrengthSum += currentCycle * registerX;
  }
  
  if (line == 'noop') {
    currentCycle++;
  }
  else {
    currentCycle++;
    if (relevantCycles.has(currentCycle)) {
      signalStrengthSum += currentCycle * registerX;
    }
    registerX += parseInt(line.slice(5));
    currentCycle++;
  }
}

console.log(signalStrengthSum);