import { open } from 'node:fs/promises';

const file = await open('./input');

let score = 0;

for await (const line of file.readLines()) {
  switch (line[2]) {
    case 'X':	// lose
	  switch (line[0]) {
	    case 'A':	// rock + lose -> scissors
		  score += 3;
		  break;
		case 'B':	// paper + lose -> rock
		  score += 1;
		  break;
		case 'C':	// scissors + lose -> paper
		  score += 2;
		  break;
	  }
	  break;
	case 'Y':	// draw
	  score += 3;
	  switch (line[0]) {
		case 'A':	// rock + draw -> rock
		  score += 1;
		  break;
		case 'B':	// paper + draw -> paper
		  score += 2;
		  break;
		case 'C':	// scissors + draw -> scissors
		  score += 3;
		  break;
	  }
	  break;
	case 'Z':	// win
	  score += 6;
	  switch (line[0]) {
		case 'A':	// rock + win -> paper
		  score += 2;
		  break;
		case 'B':	// paper + win -> scissors
		  score += 3;
		  break;
		case 'C':	// scissors + win -> rock
		  score += 1;
		  break;
	  }
	  break;
  }
}

console.log(score);

// 10565 is too high
