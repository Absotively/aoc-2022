import { open } from 'node:fs/promises';

const file = await open('./input');

let score = 0;

for await (const line of file.readLines()) {
  switch (line[2]) {
    case 'X':	// rock
	  score += 1;
	  switch (line[0]) {
	    case 'A':	// rock
		  score += 3;
		  break;
		case 'B':	// paper
		  break;
		case 'C':	// scissors
		  score += 6;
		  break;
	  }
	  break;
	case 'Y':	// paper
	  score += 2;
	  switch (line[0]) {
		case 'A':	// rock
		  score += 6;
		  break;
		case 'B':	// paper
		  score += 3;
		  break;
		case 'C':	// scissors
		  break;
	  }
	  break;
	case 'Z':	// scissors
	  score += 3;
	  switch (line[0]) {
		case 'A':	// rock
		  break;
		case 'B':	// paper
		  score += 6;
		  break;
		case 'C':	// scissors
		  score += 3;
		  break;
	  }
	  break;
  }
}

console.log(score);

// 9181 is Too High
