import { open } from 'node:fs/promises';

const file = await open('./input');

let treeGrid = new Array();

for await (const line of file.readLines()) {
  treeGrid.push(line);
}

const treeGridWidth = treeGrid[0].length;

let bestScore = 0;

for (let treehouseRow = 0; treehouseRow < treeGrid.length; treehouseRow++) {
  for (let treehouseColumn = 0; treehouseColumn < treeGrid[0].length; treehouseColumn++) {
    let treehouseTreeHeight = treeGrid[treehouseRow][treehouseColumn];
    
    let leftTrees = 0;
    for (let checkingColumn = treehouseColumn - 1; checkingColumn >= 0; checkingColumn--) {
      leftTrees++;
      if (treeGrid[treehouseRow][checkingColumn] >= treehouseTreeHeight) {
        break;
      }
    }
    
    let rightTrees = 0;
    for (let checkingColumn = treehouseColumn+1; checkingColumn < treeGridWidth; checkingColumn++) {
      rightTrees++;
      if (treeGrid[treehouseRow][checkingColumn] >= treehouseTreeHeight) {
        break;
      }
    }
    
    let upTrees = 0;
    for (let checkingRow = treehouseRow - 1; checkingRow >= 0; checkingRow--) {
      upTrees++;
      if (treeGrid[checkingRow][treehouseColumn] >= treehouseTreeHeight) {
        break;
      }
    }
    
    let downTrees = 0;
    for (let checkingRow = treehouseRow + 1; checkingRow < treeGrid.length; checkingRow++) {
      downTrees++;
      if (treeGrid[checkingRow][treehouseColumn] >= treehouseTreeHeight) {
        break;
      }
    }
    
    let score = leftTrees * rightTrees * upTrees * downTrees;
    if (score > bestScore) {
      bestScore = score;
    }
  }
}

console.log(bestScore);