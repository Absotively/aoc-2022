import { open } from 'node:fs/promises';

const file = await open('./input');

let treeGrid = new Array();
let visibleGrid = new Array();

for await (const line of file.readLines()) {
  treeGrid.push(line);
  visibleGrid.push(new Array(line.length));
  visibleGrid[visibleGrid.length - 1][0] = true;
  visibleGrid[visibleGrid.length - 1][-1] = true;
}

// outer border trees
let visibleTreeCount = 2 * treeGrid.length + 2 * treeGrid[0].length - 4;

// for each row, find all non-border trees visible from the left,
// then all visible from the right

for (let row = 1; row < treeGrid.length - 1; row++) {
  let tallestHeightSeenFromLeft = treeGrid[row][0];
  let tallestColumnSeenFromLeft = 0;
  
  if (tallestHeightSeenFromLeft != '9') {
    for (let col = 1; col < treeGrid[row].length - 1; col++) {
      if (treeGrid[row][col] > tallestHeightSeenFromLeft) {
        visibleGrid[row][col] = true;
        visibleTreeCount++;
        //console.log(`from left: ${row},${col}`);
        tallestHeightSeenFromLeft = treeGrid[row][col];
        tallestColumnSeenFromLeft = col;
      }
      if (treeGrid[row][col] == '9') {
        break;
      }
    }
  }
  
  let tallestHeightSeenFromRight = treeGrid[row][treeGrid[row].length - 1];
  if (tallestHeightSeenFromRight != '9') {
    for (let col = treeGrid[row].length - 2; col > tallestColumnSeenFromLeft; col--) {
      if (treeGrid[row][col] > tallestHeightSeenFromRight) {
        visibleGrid[row][col] = true;
        visibleTreeCount++;
        //console.log(`from right: ${row},${col}`);
        tallestHeightSeenFromRight = treeGrid[row][col];
      }
      if (treeGrid[row][col] == '9') {
        break;
      }
    }
  }
}

// now do each column, checking for trees that were already counted
// could maybe combine with above but it would be a pain

for (let col = 1; col < treeGrid[0].length - 1; col++) {
  let tallestHeightSeenFromTop = treeGrid[0][col];
  let tallestRowSeenFromTop = 0;
  
  if (tallestHeightSeenFromTop != '9') {
    for (let row = 1; row < treeGrid.length - 1; row++) {
      if (treeGrid[row][col] > tallestHeightSeenFromTop) {
        if (visibleGrid[row][col] != true) {
          visibleGrid[row][col] = true;
          visibleTreeCount++;
        //console.log(`from top: ${row},${col}`);
        }
        tallestHeightSeenFromTop = treeGrid[row][col];
        tallestRowSeenFromTop = row;
      }
      if (treeGrid[row][col] == '9') {
        break;
      }
    }
  }
  
  let tallestHeightSeenFromBottom = treeGrid[treeGrid.length - 1][col];
  if (tallestHeightSeenFromBottom != '9') {
    for (let row = treeGrid.length - 2; row > tallestRowSeenFromTop; row--) {
      if (treeGrid[row][col] > tallestHeightSeenFromBottom) {
        if (visibleGrid[row][col] != true) {
          visibleGrid[row][col] = true;
          visibleTreeCount++;
        //console.log(`from bottom: ${row},${col}`);
        }
        tallestHeightSeenFromBottom = treeGrid[row][col]
      }
      if (treeGrid[row][col] == '9') {
        break;
      }
    }
  }
}

console.log(visibleTreeCount);

// 1539 is wrong
// 1536 is also wrong
// 1540? wrong!