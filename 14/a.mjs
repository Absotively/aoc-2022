import { open } from 'node:fs/promises';

const file = await open('./input');

let columns = new Array();

function setTile(x, y, value) {
  if (columns[x] === undefined) {
    columns[x] = new Array();
  }
  columns[x][y] = value;
}

function tileEmpty(x, y) {
  if (columns[x] === undefined) {
    return true;
  }
  return columns[x][y] === undefined;
}

function tileInAbyss(x, y) {
  if (columns[x] === undefined) {
    return true;
  }
  return y > columns[x].length;
}

for await (const line of file.readLines()) {
  let points = line.split(' -> ');
  let pointValues = points[0].split(',');
  let lastX = parseInt(pointValues[0]);
  let lastY = parseInt(pointValues[1]);
  for (const point of points.slice(1)) {
    pointValues = point.split(',');
    let x = parseInt(pointValues[0]);
    let y = parseInt(pointValues[1]);
    if (x == lastX) {
      // vertical line
      let direction = Math.sign(y - lastY);
      for (let it = lastY; it != y; it += direction) {
        setTile(x, it, '#');
      }
    } else {
      // horizontal line
      let direction = Math.sign(x - lastX);
      for (let it = lastX; it != x; it += direction) {
        setTile(it, y, '#');
      }
    }
    lastX = x;
    lastY = y;
  }
  // end tile
  setTile(lastX, lastY, '#');
}

let sandFallingFreely = false;
let restingSandCount = 0;

while (!sandFallingFreely) {
  let sandX = 500;
  let sandY = 0;
  let sandLanded = false;
  while (!sandLanded) {
    if (tileInAbyss(sandX, sandY)) {
      sandFallingFreely = true;
      break;
    } else if (tileEmpty(sandX, sandY + 1)) {
      sandY++;
    } else if (tileEmpty(sandX - 1, sandY + 1)) {
      sandX--;
      sandY++;
    } else if (tileEmpty(sandX + 1, sandY + 1)) {
      sandX++;
      sandY++;
    } else {
      setTile(sandX, sandY, 'o');
      sandLanded = true;
      restingSandCount++;
    }
  }
}

console.log(restingSandCount);

// 171 is too low