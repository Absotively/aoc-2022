import { open } from 'node:fs/promises';

const file = await open('./input');

const codePointa = "a".codePointAt(0);
const codePointA = "A".codePointAt(0);
function priority(character) {
  if (character.toUpperCase() == character) {
    return (character.codePointAt(0) - codePointA + 27);
  }
  else
  {
    return (character.codePointAt(0) - codePointa + 1);
  }
}

let prioritiesSum = 0;

let elfOne = '';
let elfTwo = '';

for await (const line of file.readLines()) {
  if (elfOne == '') {
    elfOne = line;
  }
  else if (elfTwo == '') {
    elfTwo = line;
  }
  else {
    let elfThree = line;
	let possibleBadges = '';
	for (const item of elfOne) {
	  if (elfTwo.includes(item)) {
	    possibleBadges += item
	  }
	}
	for (const item of possibleBadges) {
	  if (elfThree.includes(item)) {
	    prioritiesSum += priority(item);
		break;
	  }
	}
	elfOne = '';
	elfTwo = '';
  }
}

console.log(prioritiesSum);