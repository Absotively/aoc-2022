import { open } from 'node:fs/promises';

const file = await open('./input');

const codePointa = "a".codePointAt(0);
const codePointA = "A".codePointAt(0);
function priority(character) {
  if (character.toUpperCase() == character) {
    return (character.codePointAt(0) - codePointA + 27);
  }
  else
  {
    return (character.codePointAt(0) - codePointa + 1);
  }
}

let prioritiesSum = 0;

for await (const line of file.readLines()) {
  let compartmentOne = line.substring(0, line.length / 2);
  let compartmentTwo = line.substring(line.length / 2);
  for (const item of compartmentOne) {
    if (compartmentTwo.includes(item)) {
	  prioritiesSum += priority(item);
	  break;
	}
  }
}

console.log(prioritiesSum);