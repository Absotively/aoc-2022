import { open } from 'node:fs/promises';

const file = await open('./input');

class Valve {
  constructor(name, flowRate, tunnels) {
    this.name = name
    this.flowRate = flowRate;
    this.tunnels = tunnels;
  }
}

let valveGraph = new Map();
let startingValveName = 'AA';
let usefulValveCount = 0;

const inputRE = /Valve (?<name>..) has flow rate=(?<rate>\d+); tunnels? leads? to valves? (?<tunnels>.*)/

for await (const line of file.readLines()) {
  let match = inputRE.exec(line);
  let name = match.groups.name;
  let flowRate = parseInt(match.groups.rate);
  let tunnels = match.groups.tunnels.split(', ');
  valveGraph.set(name, new Valve(name, flowRate, tunnels));
  if (flowRate > 0) {
    usefulValveCount++;
  }
}

let exploreResults = new Map();

function addExploreResult(stateString, minutesRemaining, result) {
  if (minutesRemaining < 5) {
    // fast-ish to recalculate and the map gets Too Big if we save them
    return;
  }
  if (!exploreResults.has(stateString)) {
    exploreResults.set(stateString, new Array());
  }
  let resultSet = exploreResults.get(stateString);
  resultSet[minutesRemaining] = result;
}

function getExploreResult(stateString, minutesRemaining) {
  if (!exploreResults.has(stateString)) {
    return undefined;
  }
  let resultSet = exploreResults.get(stateString);
  
  if (resultSet[minutesRemaining] !== undefined) {
    return resultSet[minutesRemaining];
  }
  
  // if we're repeating a state with less time left, we're going to do worse than it
  // so just return 0
  if (resultSet.length > minutesRemaining) {
    return 0;
  }
  
  return undefined;
}

// return maximum pressure released
function explore(myLocation, elephantLocation, openValves, currentFlowRate, minutesRemaining) {
  if (minutesRemaining <= 0 ) {
    return 0;
  }
  
  if (openValves.size == usefulValveCount) {
    return minutesRemaining * currentFlowRate;
  }
  
  let myValve = valveGraph.get(myLocation);
  let elephantValve = valveGraph.get(elephantLocation);
  
  if (minutesRemaining == 1) {
    // can't open another valve in time for it to help
    return currentFlowRate;
  }
  
  if (minutesRemaining == 2) {
    // no point going to another valve, wouldn't be able to open it in time for it to help
    let pressureReleased = currentFlowRate * 2; // released by already open valves
    if (myValve.flowRate != 0 && !openValves.has(myValve.name)) {
      pressureReleased += myValve.flowRate; // released in last minute
    }
    if (elephantLocation != myLocation && elephantValve.flowRate != 0 && !openValves.has(elephantLocation)) {
      pressureReleased += elephantValve.flowRate; // released in last minute
    }
    return pressureReleased;
  }
  
  let openValvesArray = [...openValves];
  openValvesArray.sort();
  let locations = [myLocation, elephantLocation];
  locations.sort();
  let stateString = JSON.stringify([locations, openValvesArray]);
  let oldResult = getExploreResult(stateString, minutesRemaining);
  if (oldResult !== undefined) {
    return oldResult;
  }
  
  let bestExplorationResult = 0;
  
  // I open my valve, elephant moves
  if (myValve.flowRate != 0 && !openValves.has(myLocation)) {
    let newOpenValves = new Set(openValves);
    newOpenValves.add(myLocation);
    let newFlowRate = currentFlowRate + myValve.flowRate;
    for (const nextElephantLocation of elephantValve.tunnels) {
      let explorationResult = explore(myLocation, nextElephantLocation, newOpenValves, newFlowRate, minutesRemaining - 1);
      if (explorationResult > bestExplorationResult) {
        bestExplorationResult = explorationResult;
      }
    }
  }
  
  // Elephant opens its valve, I move
  // skip if we're in the same location, since it doesn't matter who does which
  if (elephantValve.flowRate != 0 && elephantLocation != myLocation && !openValves.has(elephantLocation)) {
    let newOpenValves = new Set(openValves);
    newOpenValves.add(elephantLocation);
    let newFlowRate = currentFlowRate + elephantValve.flowRate;
    for (const myNextLocation of myValve.tunnels) {
      let explorationResult = explore(myNextLocation, elephantLocation, newOpenValves, newFlowRate, minutesRemaining - 1);
      if (explorationResult > bestExplorationResult) {
        bestExplorationResult = explorationResult;
      }
    }
  }
  
  // We both open our valves
  if (myValve.flowRate != 0 && elephantValve.flowRate != 0 && myLocation != elephantLocation
      && !openValves.has(myLocation) && !openValves.has(elephantLocation))
  {
    let newOpenValves = new Set(openValves);
    newOpenValves.add(myLocation);
    newOpenValves.add(elephantLocation);
    let newFlowRate = currentFlowRate + myValve.flowRate + elephantValve.flowRate;
    let explorationResult = explore(myLocation, elephantLocation, newOpenValves, newFlowRate, minutesRemaining - 1);
    if (explorationResult > bestExplorationResult) {
      bestExplorationResult = explorationResult;
    }
  }
  
  // We both move
  for (const myNextLocation of myValve.tunnels) {
    for (const nextElephantLocation of elephantValve.tunnels) {
      let explorationResult = explore(myNextLocation, nextElephantLocation, openValves, currentFlowRate, minutesRemaining - 1);
      if (explorationResult > bestExplorationResult) {
        bestExplorationResult = explorationResult;
      }
      }
  }
  
  bestExplorationResult += currentFlowRate; // pressure released while acting
  
  addExploreResult(stateString, minutesRemaining, bestExplorationResult);
  return bestExplorationResult;
}

console.log(explore(startingValveName, startingValveName, new Set(), 0, 26));