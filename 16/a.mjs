import { open } from 'node:fs/promises';

const file = await open('./input');

class Valve {
  constructor(name, flowRate, tunnels) {
    this.name = name
    this.flowRate = flowRate;
    this.tunnels = tunnels;
  }
}

let valveGraph = new Map();
let startingValveName = 'AA';

const inputRE = /Valve (?<name>..) has flow rate=(?<rate>\d+); tunnels? leads? to valves? (?<tunnels>.*)/

for await (const line of file.readLines()) {
  let match = inputRE.exec(line);
  let name = match.groups.name;
  let flowRate = parseInt(match.groups.rate);
  let tunnels = match.groups.tunnels.split(', ');
  valveGraph.set(name, new Valve(name, flowRate, tunnels));
  if (startingValveName == undefined) {
    startingValveName = name;
  }
}

let exploreResults = new Map();

// return maximum pressure released
function explore(locationName, openValves, currentFlowRate, minutesRemaining) {
  if (minutesRemaining <= 0 ) {
    return 0;
  }
  
  let valve = valveGraph.get(locationName);
  
  if (minutesRemaining < 3) {
    // can't get to another valve, open it, and have it release any pressure in the remaining time
    if (valve.flowRate != 0 && minutesRemaining == 2 && !openValves.has(locationName)) {
      return (2 * currentFlowRate) + valve.flowRate;
    } else {
      return minutesRemaining * currentFlowRate;
    }
  }
  
  let argsString = JSON.stringify([locationName, [...openValves], currentFlowRate, minutesRemaining]);
  if (exploreResults.has(argsString)) {
    return exploreResults.get(argsString);
  }
  
  let bestExplorationResult = 0;
  
  // try opening this valve and then exploring
  if (valve.flowRate != 0 && !openValves.has(locationName)) {
    let newOpenValves = new Set(openValves);
    newOpenValves.add(locationName);
    let newFlowRate = currentFlowRate + valve.flowRate;
    for (const nextValveName of valve.tunnels) {
      let explorationResult = explore(nextValveName, newOpenValves, newFlowRate, minutesRemaining - 2);
      explorationResult += newFlowRate;  // pressure released while traversing the tunnel
      if (explorationResult > bestExplorationResult) {
        bestExplorationResult = explorationResult;
      }
    }
    bestExplorationResult += currentFlowRate; // pressure released while opening the valve
  }
  
  // try exploring without opening the valve
  for (const nextValveName of valve.tunnels) {
    let explorationResult = explore(nextValveName, openValves, currentFlowRate, minutesRemaining - 1);
    explorationResult += currentFlowRate;  // pressure released while traversing the tunnel
    if (explorationResult > bestExplorationResult) {
      bestExplorationResult = explorationResult;
    }
  }
  
  exploreResults.set(argsString, bestExplorationResult);
  return bestExplorationResult;
}

console.log(explore(startingValveName, new Set(), 0, 30));