import { open } from 'node:fs/promises';

const file = await open('./input');

let currentElfSum = 0;
let mostCaloriesElfSum = 0;

for await (const line of file.readLines()) {
  if (line.length == 0) {
    mostCaloriesElfSum = Math.max(currentElfSum, mostCaloriesElfSum);
	currentElfSum = 0;
  } else {
    currentElfSum += parseInt(line);
  }
}

console.log(Math.max(currentElfSum, mostCaloriesElfSum));