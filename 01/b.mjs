import { open } from 'node:fs/promises';

const file = await open('./input');

let currentElfSum = 0;
let sums = new Array();

for await (const line of file.readLines()) {
  if (line.length == 0) {
	sums.push(currentElfSum);
	currentElfSum = 0;
  } else {
    currentElfSum += parseInt(line);
  }
}
if (currentElfSum != 0) {
  sums.push(currentElfSum);
}

sums = sums.sort((a,b) => { return b - a; });

console.log(sums[0] + sums[1] + sums[2]);