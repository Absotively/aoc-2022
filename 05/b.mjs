import { open } from 'node:fs/promises';

const file = await open('./input');

let stacks = new Array();

let stacksRead = false;

let stepRE = /move (\d+) from (\d+) to (\d+)/

for await (const line of file.readLines()) {
  if (!stacksRead) {
    for (let i = 1; i < line.length; i += 4) {
      if (line[i] == '1') {
        stacksRead = true;
        break;
      }
      else if (line[i] != ' ') {
        let column = Math.floor(i / 4);
        if (stacks[column] === undefined) {
          stacks[column] = new Array();
        }
        stacks[column].unshift(line[i]);
      }
    }
  }
  else {
    let reMatch = stepRE.exec(line);
    if (reMatch !== null) {
      let count = parseInt(reMatch[1]);
      let fromStack = parseInt(reMatch[2]) - 1;
      let toStack = parseInt(reMatch[3]) - 1;
      stacks[toStack] = stacks[toStack].concat(stacks[fromStack].slice(-1 * count));
      stacks[fromStack].splice(-1 * count, count);
    }
  }
}

let output = '';

for (let stack of stacks) {
  output += stack[stack.length - 1];
}

console.log(output);