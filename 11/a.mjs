import { open } from 'node:fs/promises';
import { Monkey } from './monkey.mjs';

const file = await open('./input');

const input = await file.readFile('ascii');

let monkeys = new Array();

for (let monkeyDef of input.split('\n\n')) {
  let monkey = new Monkey(monkeyDef);
  monkeys[monkey.number] = monkey;
}

function throwCallback(destination, itemWorryLevel) {
  monkeys[destination].items.push(itemWorryLevel);
}

for (let i = 0; i < 20; i++) {
  for (let monkey of monkeys) {
    monkey.inspectAndThrowItems(throwCallback);
  }
}

let inspectionCounts = monkeys.map(x => x.inspectionCount).sort((a,b) => b - a);

console.log(inspectionCounts[0] * inspectionCounts[1]);