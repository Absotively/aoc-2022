class Monkey {
  static divisorsProduct = BigInt(1);
  
  constructor(text) {
    let lines = text.split('\n');
    this.number = parseInt(lines[0].slice(7));
    this.items = lines[1].slice(18).split(', ').map(x => BigInt(parseInt(x)));
    this.operator = lines[2][23];
    let rawOperand = lines[2].slice(25);
    if (rawOperand == 'old') {
      this.operandIsOldValue = true;
      this.operandNumericValue = undefined;
    } else {
      this.operandIsOldValue = false;
      this.operandNumericValue = BigInt(parseInt(rawOperand));
    }
    this.testDivisor = BigInt(parseInt(lines[3].slice(21)));
    if (Monkey.divisorsProduct % this.testDivisor) {
      Monkey.divisorsProduct *= this.testDivisor;
    }
    this.ifTrueDestination = parseInt(lines[4].slice(29));
    this.ifFalseDestination = parseInt(lines[5].slice(30));
    this.inspectionCount = 0;
  }
  
  inspectAndThrowItems(throwCallback, reliefFelt = true) {
    for (let item of this.items) {
      // inspect item
      this.inspectionCount++;
      if (this.operandIsOldValue) {
        if (this.operator == '+') {
          item *= 2;
        } else if (this.operator == '*') {
          item = item * item;
        }
      } else {
        if (this.operator == '+') {
          item += this.operandNumericValue;
        } else if (this.operator == '*') {
          item *= this.operandNumericValue;
        }
      }
      
      // relief and/or worry level management
      if (reliefFelt) {
        item = Math.floor(item / 3);
      } else {
        item = item % Monkey.divisorsProduct;
      }
      
      // test & throw
      if (item % this.testDivisor == 0) {
        throwCallback(this.ifTrueDestination, item);
      } else {
        throwCallback(this.ifFalseDestination, item);
      }
    }
    
    this.items = new Array();
  }
}

export { Monkey };