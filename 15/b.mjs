import { open } from 'node:fs/promises';

const file = await open('./input');
const searchRange = 4000000;
//const file = await open('./example');
//const searchRange = 20;

class Sensor {
  constructor(x, y, beaconDistance) {
    this.x = x;
    this.y = y;
    this.beaconDistance = beaconDistance;
  }
  
  coveredRangeInRow(row) {
    let rowDistance = Math.abs(this.y - row);
    if (this.beaconDistance > rowDistance) {
      let halfWidthAtRow = this.beaconDistance - rowDistance;
      let rangeStart = this.x - halfWidthAtRow;
      let rangeEnd = this.x + halfWidthAtRow;
      return [rangeStart, rangeEnd];
    } else {
      return undefined;
    }
  }
  
  toString() {
    return `(${this.x},${this.y})\t${this.beaconDistance}`;
  }
}

let sensors = [];

const inputRE = /Sensor at x=(?<sensorX>-?\d+), y=(?<sensorY>-?\d+): closest beacon is at x=(?<beaconX>-?\d+), y=(?<beaconY>-?\d+)/

for await (const line of file.readLines()) {
  let match = inputRE.exec(line);
  let sensorX = parseInt(match.groups.sensorX);
  let sensorY = parseInt(match.groups.sensorY);
  let beaconX = parseInt(match.groups.beaconX);
  let beaconY = parseInt(match.groups.beaconY);
  let beaconDistance = Math.abs(sensorX - beaconX) + Math.abs(sensorY - beaconY);
  sensors.push(new Sensor(sensorX, sensorY, beaconDistance));
}

for (let row = 0; row <= searchRange; row++) {
  let coveredRanges = [];
  for (const sensor of sensors) {
    let range = sensor.coveredRangeInRow(row);
    if (range !== undefined) {
      coveredRanges.push(range);
    }
  }
  let uncovered = subtractRangeGroups([[0, searchRange]], coveredRanges);
  if (uncovered.length != 0) {
    console.log(uncovered[0][0] * 4000000 + row);
    break;
  }
}

function subtractRanges(minuend, subtrahend) {
  // thanks https://www.classace.io/learn/math/3rdgrade/terms-for-addition-subtraction-multiplication-division-equations
  if (subtrahend[0] <= minuend[0] && subtrahend[1] >= minuend[1]) {
    return [];
  }
  if (subtrahend[0] <= minuend[0] && subtrahend[1] >= minuend[0]) {
    return [[subtrahend[1] + 1, minuend[1]]];
  }
  if (subtrahend[0] <= minuend[1] && subtrahend[1] >= minuend[1]) {
    return [[minuend[0], subtrahend[0] - 1]];
  }
  if (subtrahend[0] > minuend [0] && subtrahend[1] < minuend[1]) {
    return [[minuend[0], subtrahend[0] - 1], [subtrahend[1] + 1, minuend[1]]];
  }
  else {
    return [minuend];
  }
}

function subtractRangeGroups(minuends, subtrahends) {
  let result = [...minuends];
  for (const sub of subtrahends) {
    let newResult = [];
    for (let min of result) {
      newResult = newResult.concat(subtractRanges(min, sub));
    }
    result = newResult;
  }
  return result;
}