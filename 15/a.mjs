import { open } from 'node:fs/promises';

const file = await open('./input');

const measurementRow = 2000000;

let coveredRanges = [];

const inputRE = /Sensor at x=(?<sensorX>-?\d+), y=(?<sensorY>-?\d+): closest beacon is at x=(?<beaconX>-?\d+), y=(?<beaconY>-?\d+)/

for await (const line of file.readLines()) {
  let match = inputRE.exec(line);
  let sensorX = parseInt(match.groups.sensorX);
  let sensorY = parseInt(match.groups.sensorY);
  let beaconX = parseInt(match.groups.beaconX);
  let beaconY = parseInt(match.groups.beaconY);
  let beaconDistance = Math.abs(sensorX - beaconX) + Math.abs(sensorY - beaconY);
  let measurementRowDistance = Math.abs(sensorY - measurementRow);
  if (beaconDistance > measurementRowDistance) {
    let halfWidthAtMeasurementRow = beaconDistance - measurementRowDistance;
    let rangeStart = sensorX - halfWidthAtMeasurementRow;
    let rangeEnd = sensorX + halfWidthAtMeasurementRow;
    coveredRanges.push([rangeStart, rangeEnd]);
  }
}

// merge the ranges
let mergedCoveredRanges = [];
let inMergedRanges = new Set();

for (let i = 0; i < coveredRanges.length; i++) {
  if (inMergedRanges.has(i)) {
    continue;
  }
  let currentMerge = [...coveredRanges[i]];
  let checkedAll = false;
  if (i != coveredRanges.length - 1) {
    while (!checkedAll) {
      for (let j = i+1; j < coveredRanges.length; j++) {
        if (inMergedRanges.has(j)) {
          if (j == coveredRanges.length - 1) {
            checkedAll = true;
          }
          continue;
        }
        if (coveredRanges[j][0] <= currentMerge[1] && coveredRanges[j][1] >= currentMerge[0]) {
          currentMerge[0] = Math.min(currentMerge[0], coveredRanges[j][0]);
          currentMerge[1] = Math.max(currentMerge[1], coveredRanges[j][1]);
          inMergedRanges.add(j);
          break;  // go back in case the newly merged range could merge with an already-checked range
        }
        if (j == coveredRanges.length - 1) {
          checkedAll = true;
        }
      }
    }
  }
  mergedCoveredRanges.push(currentMerge);
}

let excludedLocations = 0;
for (const range of mergedCoveredRanges) {
  excludedLocations += range[1] - range[0];
}

console.log(excludedLocations);

// 8607424 is too high