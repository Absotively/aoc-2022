import { open } from 'node:fs/promises';

const file = await open('./input');

let count = 0;

const pairRE = /(\d+)-(\d+),(\d+)-(\d+)/

for await (const line of file.readLines()) {
  let bounds = pairRE.exec(line);
  let elfALower = parseInt(bounds[1]);
  let elfAUpper = parseInt(bounds[2]);
  let elfBLower = parseInt(bounds[3]);
  let elfBUpper = parseInt(bounds[4]);
  if ((elfBLower <= elfAUpper && elfBUpper >= elfALower)
      || (elfALower <= elfBUpper && elfAUpper >= elfBLower)) {
      count++;
  }
}

console.log(count);

// 500 is wrong
// 492 is also wrong