import { open } from 'node:fs/promises';

const file = await open('./input');

const input = await file.readFile('ascii');

const markerLength = 14;

for (let i = 0; i < input.length; i++) {
  let positionFound = true;
  for (let j = 0; j < markerLength; j++) {
    for (let k = j + 1; k < markerLength; k++) {
      if (input[i+j] == input[i+k]) {
        positionFound = false;
        break;
      }
    }
    if (!positionFound) {
      break;
    }
  }
  if (positionFound) {
    console.log(i + markerLength);
    break;
  }
}