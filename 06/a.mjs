import { open } from 'node:fs/promises';

const file = await open('./input');

const input = await file.readFile('ascii');

for (let i = 4; i < input.length; i++) {
  if (input[i-4] != input[i-3] && input[i-4] != input[i-2] && input[i-4] != input[i-1]
      && input[i-3] != input[i-2] && input[i-3] != input[i-1]
      && input[i-2] != input[i-1]) {
    console.log(i);
    break;
  }
}