import { open } from 'node:fs/promises';

const file = await open('./input');
const input = await file.readFile('ascii');

const digits = '0123456789';

let packetPairIndex = 1;
let orderedPairIndexSum = 0;

for (const pairText of input.split('\n\n')) {
  let packetsText = pairText.split('\n');
  let leftPacket = readPacket(packetsText[0]);
  let rightPacket = readPacket(packetsText[1]);
  if (comparePacketValues(leftPacket, rightPacket) == -1) {
    orderedPairIndexSum += packetPairIndex;
  }
  packetPairIndex++;
}

console.log(orderedPairIndexSum);

function readList(text, startingIndex) {
  let value = new Array();
  
  let index = startingIndex + 1;  // skip opening [
  
  while (index < text.length) {
    if (text[index] == '[') {
      let sublist = readList(text, index);
      index = sublist.nextIndex;
      value.push(sublist.value);
    } else if (text[index] == ']') {
      return { value: value, nextIndex: index + 1 };
    } else if (digits.includes(text[index])) {
      value.push(parseInt(text.slice(index)));
      while (digits.includes(text[index])) {
        index++;
      }
    } else if (text[index] == ',') {
      index++;
    } else {
      console.log("list reading error");
    }
  }
}

function readPacket(text) {
  return readList(text, 0).value;
}

function comparePacketValues(left, right) {
  if (typeof left == 'number' && typeof right == 'number') {
    return Math.sign(left - right);
  }
  else if (typeof left == "object" && typeof right == "object") {
    for (let i = 0; i < left.length && i < right.length; i++) {
      let itemComp = comparePacketValues(left[i], right[i]);
      if (itemComp != 0) {
        return itemComp;
      }
    }
    return Math.sign(left.length - right.length);
  }
  else if (typeof left == "number") {
    return comparePacketValues([left], right);
  } else if (typeof right == "number") {
    return comparePacketValues(left, [right]);
  } else {
    console.log("comparison error");
  }
}