import { open } from 'node:fs/promises';

const file = await open('./input');

let cubes = [];

let exposedSides = 0;

for await (const line of file.readLines()) {
  let newCube = line.split(',').map(x => parseInt(x));
  exposedSides += 6;
  for (let i = 0; i < cubes.length; i++) {
    if (cubes[i][0] == newCube[0] && cubes[i][1] == newCube[1] && Math.abs(cubes[i][2] - newCube[2]) == 1
        || cubes[i][0] == newCube[0] && Math.abs(cubes[i][1] - newCube[1]) == 1 && cubes[i][2] == newCube[2]
        || Math.abs(cubes[i][0] - newCube[0]) == 1 && cubes[i][1] == newCube[1] && cubes[i][2] == newCube[2]
    ) {
      exposedSides -= 2;
    }
  }
  cubes.push(newCube);
}

console.log(exposedSides);