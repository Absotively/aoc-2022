import { open } from 'node:fs/promises';

const file = await open('./input');

let cubes = [];

let scannedVolume = [];
let xMax = 0;
let yMax = 0;
let zMax = 0

function setVoxel(x, y, z, value) {
  if (scannedVolume[x] === undefined) {
    scannedVolume[x] = [];
  }
  if (scannedVolume[x][y] === undefined) {
    scannedVolume[x][y] = [];
  }
  scannedVolume[x][y][z] = value;
  xMax = Math.max(x, xMax);
  yMax = Math.max(y, yMax);
  zMax = Math.max(z, zMax);
  if (isNaN(xMax) || isNaN(yMax) || isNaN(zMax)) {
    console.log(`${x},${y},${z}\t${value}`);
  }
}

function getVoxel(x, y, z) {
  if (x < 0 || y < 0 || z < 0) {
    return undefined;
  }
  if (scannedVolume[x] === undefined) {
    return undefined;
  }
  if (scannedVolume[x][y] === undefined) {
    return undefined;
  }
  return scannedVolume[x][y][z];
}

const lava = '#';
const untrappedAir = '.';
const trappedAir = '@';

let exposedSides = 0;

for await (const line of file.readLines()) {
  let newCube = line.split(',').map(x => parseInt(x));
  exposedSides += 6;
  for (let i = 0; i < cubes.length; i++) {
    if (cubes[i][0] == newCube[0] && cubes[i][1] == newCube[1] && Math.abs(cubes[i][2] - newCube[2]) == 1
        || cubes[i][0] == newCube[0] && Math.abs(cubes[i][1] - newCube[1]) == 1 && cubes[i][2] == newCube[2]
        || Math.abs(cubes[i][0] - newCube[0]) == 1 && cubes[i][1] == newCube[1] && cubes[i][2] == newCube[2]
    ) {
      exposedSides -= 2;
    }
  }
  setVoxel(newCube[0], newCube[1], newCube[2], lava);
  cubes.push(newCube);
}

console.log('scanned lava');
console.log(exposedSides);
printVolumeAsSlices();

// now mark untrapped air, which is either on the edge of the scanned volume or connected to air that is
// don't care if cubes get added multiple times
let possibleUntrappedAirCubes = [];
for (let x = 1; x <= xMax; x++) {
  for (let y = 1; y <= yMax; y++) {
    possibleUntrappedAirCubes.push([x, y, 1]);
    possibleUntrappedAirCubes.push([x, y, zMax]);
  }
  for (let z = 1; z <= zMax; z++) {
    possibleUntrappedAirCubes.push([x, 1, z]);
    possibleUntrappedAirCubes.push([x, yMax, z]);
  }
}
for (let y = 1; y <= yMax; y++) {
  for (let z = 1; z <= zMax; z++) {
    possibleUntrappedAirCubes.push([1, y, z]);
    possibleUntrappedAirCubes.push([xMax, y, z]);
  }
}
while (possibleUntrappedAirCubes.length > 0) {
  let cube = possibleUntrappedAirCubes.pop();
  if (getVoxel(cube[0], cube[1], cube[2]) === undefined) {
    setVoxel(cube[0], cube[1], cube[2], untrappedAir);
    if (cube[0] > 1) {
      possibleUntrappedAirCubes.push([cube[0] - 1, cube[1], cube[2]]);
    }
    if (cube[0] < xMax) {
      possibleUntrappedAirCubes.push([cube[0] + 1, cube[1], cube[2]]);
    }
    if (cube[1] > 1) {
      possibleUntrappedAirCubes.push([cube[0], cube[1] - 1, cube[2]]);
    }
    if (cube[1] < yMax) {
      possibleUntrappedAirCubes.push([cube[0], cube[1] + 1, cube[2]]);
    }
    if (cube[2] > 1) {
      possibleUntrappedAirCubes.push([cube[0], cube[1], cube[2] - 1]);
    }
    if (cube[2] < zMax) {
      possibleUntrappedAirCubes.push([cube[0], cube[1], cube[2] + 1]);
    }
  }
}

console.log('found untrapped air');
printVolumeAsSlices();

// now scan for trapped air, and remove lava sides adjacent to it from exposed sides count
for (let x = 1; x <= xMax; x++) {
  for (let y = 1; y <= yMax; y++) {
    for (let z = 1; z <= zMax; z++) {
      if (getVoxel(x, y, z) === undefined) {
        setVoxel(x, y, z, trappedAir);
        if (getVoxel(x - 1, y, z) == lava) {
          console.log('lava at', x-1,y,z);
          console.log('next to',x,y,z);
          exposedSides--;
        }
        if (getVoxel(x + 1, y, z) == lava) {
          console.log('lava at', x+1,y,z);
          console.log('next to',x,y,z);
          exposedSides--;
        }
        if (getVoxel(x, y - 1, z) == lava) {
          console.log('lava at', x,y-1,z);
          console.log('next to',x,y,z);
          exposedSides--;
        }
        if (getVoxel(x, y + 1, z) == lava) {
          console.log('lava at', x,y+1,z);
          console.log('next to',x,y,z);
          exposedSides--;
        }
        if (getVoxel(x, y, z - 1) == lava) {
          console.log('lava at', x,y,z-1);
          console.log('next to',x,y,z);
          exposedSides--;
        }
        if (getVoxel(x, y, z + 1) == lava) {
          console.log('lava at', x,y,z+1);
          console.log('next to',x,y,z);
          exposedSides--;
        }
      }
    }
  }
}

console.log('found trapped air');
printVolumeAsSlices();

function printVolumeAsSlices() {
  for (let x = 1; x <= xMax; x++) {
    for (let y = 1; y <= yMax; y++) {
      let sliceRow = '';
      for (let z = 1; z <= zMax; z++) {
        let value = getVoxel(x, y, z);
        if (value !== undefined) {
          sliceRow += value;
        } else {
          sliceRow += ' ';
        }
      }
      console.log(sliceRow);
    }
    console.log('');
  }
}

console.log(exposedSides);

// look specifically for lava sides next to exposed air or the edge, see if I get a different number
exposedSides = 0;
for (const lavaCube of cubes) {
  if (lavaCube[0] == 1 || getVoxel(lavaCube[0] - 1, lavaCube[1], lavaCube[2]) == untrappedAir) {
    exposedSides++;
  }
  if (lavaCube[0] == xMax || getVoxel(lavaCube[0] + 1, lavaCube[1], lavaCube[2]) == untrappedAir) {
    exposedSides++;
  }
  if (lavaCube[1] == 1 || getVoxel(lavaCube[0], lavaCube[1] - 1, lavaCube[2]) == untrappedAir) {
    exposedSides++;
  }
  if (lavaCube[1] == yMax || getVoxel(lavaCube[0], lavaCube[1] + 1, lavaCube[2]) == untrappedAir) {
    exposedSides++;
  }
  if (lavaCube[2] == 1 || getVoxel(lavaCube[0], lavaCube[1], lavaCube[2] - 1) == untrappedAir) {
    exposedSides++;
  }
  if (lavaCube[2] == zMax || getVoxel(lavaCube[0], lavaCube[1], lavaCube[2] + 1) == untrappedAir) {
    exposedSides++;
  }
}

console.log(exposedSides);

// 2582 is too high
// 2548 is too high