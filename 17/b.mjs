import { open } from 'node:fs/promises';
//import { writeHeapSnapshot } from 'node:v8';

const file = await open('./input');
const rawInput = await file.readFile('ascii');
const input = rawInput.trim();

let columns = new Array(7);
for (let i = 0; i < 7; i++) {
  columns[i] = [];
}
let discardedRows = 0;
const rockBatchSize = 1000000000;

// x, y coords of each non-empty tile, from lower left of rock
const rocks = [
[[0,0],[1,0],[2,0],[3,0]],
[[1,0],[0,1],[1,1],[2,1],[1,2]],
[[0,0],[1,0],[2,0],[2,1],[2,2]],
[[0,0],[0,1],[0,2],[0,3]],
[[0,0],[1,0],[0,1],[1,1]]
];

let fallingRockIndex = 0;
let fallingRockLocation = [2,3];
let jetIndex = 0;
let stoppedRockCount = 0;

while (stoppedRockCount < 1000000000000) {
  let rock = rocks[fallingRockIndex];
  // jet first
  if (input[jetIndex] == '<') {
    let destination = [fallingRockLocation[0] - 1, fallingRockLocation[1]];
    if (canRockMove(rock, destination)) {
      fallingRockLocation = destination;
    }
  }
  else {
    let destination = [fallingRockLocation[0] + 1, fallingRockLocation[1]];
    if (canRockMove(rock, destination)) {
      fallingRockLocation = destination;
    }
  }
  
  // now gravity
  let destination = [fallingRockLocation[0], fallingRockLocation[1] - 1];
  if (canRockMove(rock, destination)) {
    fallingRockLocation = destination;
  }
  else {
    // rock stops
    for (const rockPart of rock) {
      columns[rockPart[0]+fallingRockLocation[0]][rockPart[1]+fallingRockLocation[1]] = '#';
    }
    stoppedRockCount++;
    if (stoppedRockCount % rockBatchSize == 0) {
      console.log(stoppedRockCount);
      discardRows(rockBatchSize);
    }
    
    // new rock drops
    fallingRockLocation[0] = 2;
    fallingRockLocation[1] = 3;
    for (const column of columns) {
      if (column.length + 3 > fallingRockLocation[1]) {
        fallingRockLocation[1] = column.length + 3;
      }
    }
    fallingRockIndex = (fallingRockIndex + 1) % rocks.length;
  }
  
  // next jet!
  jetIndex = (jetIndex + 1) % input.length;
}

let rockColumnHeight = 0;
for (const column of columns) {
  if (column.length > rockColumnHeight) {
    rockColumnHeight = column.length;
  }
}

console.log(rockColumnHeight + discardedRows);

function canRockMove(rock, destination) {
  // floor
  if (destination[1] < 0) {
    return false;
  }
  
  for (const rockPart of rock) {
    // walls
    if (rockPart[0] + destination[0] < 0 || rockPart[0] + destination[0] > 6) {
      return false;
    }
    
    // stopped rocks
    if (columns[rockPart[0] + destination[0]][rockPart[1] + destination[1]] !== undefined) {
      return false;
    }
  }
  
  return true;
}

function discardRows(rowsToCheck) {
  // starting from the top, find a row no rock can get into, and discard it and the rows below it
  
  // a series of five rows can't be gotten through if (a) there is at least one rock part in
  // each column and (b) there is at least one row that doesn't have two consecutive empty spots
  // (more complicated definitions might be better, I'm looking for the simplest one that suffices)
  
  // probably a simpler definition would have worked if I hadn't had unrelated bugs that I
  // assumed were due to my definition being too simple
  
  // first find current highest row of lowest column
  let highestRow = columns[0].length;
  let tallestColumnHeight = 0;
  for (const column of columns) {
    if (column.length < highestRow) {
      highestRow = column.length;
    }
    if (column.length > tallestColumnHeight) {
      tallestColumnHeight = column.length;
    }
  }
  
  let lowestRowToCheck = highestRow - rowsToCheck;
  for (let row = highestRow - 4; row >= lowestRowToCheck && row >= 0; row--) {
    let allColumnsBlocked = true;
    for (const column of columns) {
      if (column[row] === undefined
          && column[row + 1] === undefined
          && column[row + 2] === undefined
          && column[row + 3] === undefined
          && column[row + 4] === undefined
      ) {
        allColumnsBlocked = false;
        break;
      }
    }
    if (allColumnsBlocked) {
      let someRowBlocksSquare = false;
      for (let checkRow = row; checkRow < row + 5; checkRow++) {
        let lastSquareWasEmpty = false;
        let thisRowBlocksSquare = true;
        for (const column of columns) {
          if (column[checkRow] === undefined) {
            if (lastSquareWasEmpty) {
              thisRowBlocksSquare = false;
              break;
            } else {
              lastSquareWasEmpty = true;
            }
          }
        }
        if (thisRowBlocksSquare) {
          someRowBlocksSquare = true;
          break;
        }
      }
      
      if (someRowBlocksSquare) {
        // we can discard rows! huzzah!
        for (let i = 0; i < 7; i++) {
          columns[i] = columns[i].slice(row + 1);
        }
        discardedRows += row + 1;
        return;
      }
    }
  }
}

function debugPrintColumns(columns) {
  let height = 1;
  for (const column of columns) {
    height = Math.max(height, column.length);
  }
  for (let i = height - 1; i >= 0; i--) {
    let row = '';
    for (const column of columns) {
      if (column[i] == '#') {
        row += '#';
      }
      else {
        row += '.';
      }
    }
    console.log(row);
  }
  console.log('');
}

// 1623 is too low
// 3175 is too low, but right for someone else