import { open } from 'node:fs/promises';

const file = await open('./input');
const rawInput = await file.readFile('ascii');
const input = rawInput.trim();

let columns = new Array(7);
for (let i = 0; i < 7; i++) {
  columns[i] = [];
}

// x, y coords of each non-empty tile, from lower left of rock
const rocks = [
[[0,0],[1,0],[2,0],[3,0]],
[[1,0],[0,1],[1,1],[2,1],[1,2]],
[[0,0],[1,0],[2,0],[2,1],[2,2]],
[[0,0],[0,1],[0,2],[0,3]],
[[0,0],[1,0],[0,1],[1,1]]
];

let fallingRockIndex = 0;
let fallingRockLocation = [2,3];
let jetIndex = 0;
let stoppedRockCount = 0;

while (stoppedRockCount < 2022) {
  let rock = rocks[fallingRockIndex];
  // jet first
  if (input[jetIndex] == '<') {
    let destination = [fallingRockLocation[0] - 1, fallingRockLocation[1]];
    if (canRockMove(rock, destination)) {
      fallingRockLocation = destination;
    }
  }
  else {
    let destination = [fallingRockLocation[0] + 1, fallingRockLocation[1]];
    if (canRockMove(rock, destination)) {
      fallingRockLocation = destination;
    }
  }
  
  // now gravity
  let destination = [fallingRockLocation[0], fallingRockLocation[1] - 1];
  if (canRockMove(rock, destination)) {
    fallingRockLocation = destination;
  }
  else {
    // rock stops
    for (const rockPart of rock) {
      columns[rockPart[0]+fallingRockLocation[0]][rockPart[1]+fallingRockLocation[1]] = '#';
    }
    stoppedRockCount++;
    
    // new rock drops
    fallingRockLocation[0] = 2;
    fallingRockLocation[1] = 3;
    for (const column of columns) {
      if (column.length + 3 > fallingRockLocation[1]) {
        fallingRockLocation[1] = column.length + 3;
      }
    }
    fallingRockIndex = (fallingRockIndex + 1) % rocks.length;
  }
  
  // next jet!
  jetIndex = (jetIndex + 1) % input.length;
}

let rockColumnHeight = 0;
for (const column of columns) {
  if (column.length > rockColumnHeight) {
    rockColumnHeight = column.length;
  }
}

console.log(rockColumnHeight);

function canRockMove(rock, destination) {
  // floor
  if (destination[1] < 0) {
    return false;
  }
  
  for (const rockPart of rock) {
    // walls
    if (rockPart[0] + destination[0] < 0 || rockPart[0] + destination[0] > 6) {
      return false;
    }
    
    // stopped rocks
    if (columns[rockPart[0] + destination[0]][rockPart[1] + destination[1]] !== undefined) {
      return false;
    }
  }
  
  return true;
}

function debugPrintColumns() {
  let height = 1;
  for (const column of columns) {
    height = Math.max(height, column.length);
  }
  for (let i = height; i >= 0; i--) {
    let row = '';
    for (const column of columns) {
      if (column[i] == '#') {
        row += '#';
      }
      else {
        row += '.';
      }
    }
    console.log(row);
  }
  console.log('');
}

// 1623 is too low
// 3175 is too low, but right for someone else